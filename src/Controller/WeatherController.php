<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Service\WeatherUtil;
use App\Repository\LocationRepository;
use App\Entity\Location;

class WeatherController extends AbstractController
{
    #[Route('/weather/{city}', name: 'app_weather')]
    public function city(string $city, WeatherUtil $util, LocationRepository $repository): Response
    {
        $location = $repository->findByCity($city);

        if ($location != null) {
            $measurements = $util->getWeatherForLocation($location);
        } else {
            $measurements = [];
        }
        
        return $this->render('weather/city.html.twig', [
            'location' => $location,
            'measurements' => $measurements,
        ]);
    }
}
